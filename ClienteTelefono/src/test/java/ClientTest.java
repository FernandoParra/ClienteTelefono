import static org.junit.Assert.*;
import static org.mockito.Mockito.when;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class)
public class ClientTest {
@Mock
private Phone phone1, phone2;

 @InjectMocks
private Client client;

@Test
public void shouldReturnTrueIfClientHasMobile() {
when(phone1.isMobile()).thenReturn(true);
client.addPhone(phone1);
client.addPhone(phone2);
assertTrue(client.hasMobile());
}

@Test
public void shouldReturnFalseIfClientHasMobile() {
when(phone1.isMobile()).thenReturn(false);
client.addPhone(phone1);
client.addPhone(phone2);
assertTrue(client.hasMobile());
}



@Test
public void shouldReturnFalseIfClientHasNoMobile() {
when(phone2.isMobile()).thenReturn(false);
client.addPhone(phone2);
assertFalse(client.hasMobile());
 }
}

